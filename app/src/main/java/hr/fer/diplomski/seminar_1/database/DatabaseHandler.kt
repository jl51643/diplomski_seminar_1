package hr.fer.diplomski.seminar_1.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import hr.fer.diplomski.seminar_1.model.Card
import hr.fer.diplomski.seminar_1.model.Cards
import hr.fer.diplomski.seminar_1.model.Game
import kotlin.collections.ArrayList

class DatabaseHandler(context: Context): SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "Memory"
        private const val TABLE_GAME = "Games"
        private const val TABLE_CARDS = "Cards"

        private const val KEY_ID = "_id"
        private const val KEY_GAME_ID = "game_id"
        private const val KEY_GAME_NAME = "game_name"
        private const val KEY_WORD = "word"
        private const val KEY_TRANSLATION = "translation"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_CARD_TABLE = ("CREATE TABLE $TABLE_CARDS(" +
                "$KEY_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$KEY_GAME_ID INTEGER, " +
                "$KEY_WORD TEXT, " +
                "$KEY_TRANSLATION TEXT" +
                ")")

        val CREATE_GAME_TABLE = ("CREATE TABLE $TABLE_GAME(" +
                "$KEY_GAME_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$KEY_GAME_NAME TEXT" +
                ")")

        db?.execSQL(CREATE_CARD_TABLE)
        db?.execSQL(CREATE_GAME_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newversion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_GAME")
        db.execSQL("DROP TABLE IF EXISTS $TABLE_CARDS")
        onCreate(db)
    }

    fun createGame(gameName: String): Game {
        val db = this.writableDatabase

        val contentValues = ContentValues()

        contentValues.put(KEY_GAME_NAME, gameName)
        val id = db.insert(TABLE_GAME, null, contentValues)
        db.close()
        return Game(id, gameName)
    }

    fun insertCards (cards: ArrayList<Card>, gameId: Long): Cards {
        val db = this.writableDatabase

        val cardsList: ArrayList<Card> = ArrayList()

        cards.forEach {
            val contentValues = ContentValues()
            contentValues.put(KEY_GAME_ID, gameId)
            contentValues.put(KEY_WORD, it.word)
            contentValues.put(KEY_TRANSLATION, it.translation)
            val id = db.insert(TABLE_CARDS, null, contentValues)
            cardsList.add(Card(id, it.word, it.translation))
        }

        db.close()

        return Cards(gameId = gameId, cardsList)
    }

    fun getGameCards(gameId: Long): Cards {
        val cardsList: ArrayList<Card> = ArrayList()

        val query = "SELECT * FROM $TABLE_CARDS WHERE game_id = $gameId"

        val db = this.readableDatabase
        val cursor: Cursor?

        try {
            cursor = db.rawQuery(query, null)
        } catch (e: SQLException) {
            return Cards(gameId, ArrayList())
        }

        var id: Long
        var word: String
        var translation: String

        if (cursor.moveToFirst()) {
            do {
                id = cursor.getLong(cursor.getColumnIndex(KEY_ID))
                word = cursor.getString(cursor.getColumnIndex(KEY_WORD))
                translation = cursor.getString(cursor.getColumnIndex(KEY_TRANSLATION))
                cardsList.add(Card(id, word, translation))
            } while (cursor.moveToNext())
        }

        return Cards(gameId, cardsList)
    }

    fun getGames(): ArrayList<Game> {

        val gamesList: ArrayList<Game> = ArrayList()

        val selectQuery = "SELECT * FROM $TABLE_GAME"

        val db = this.readableDatabase
        var cursor: Cursor? = null

        try {
            cursor = db.rawQuery(selectQuery, null)
        } catch (e: SQLException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }

        var id: Long
        var name: String

        if (cursor.moveToFirst()) {
            do {
                id = cursor.getLong(cursor.getColumnIndex(KEY_GAME_ID))
                name = cursor.getString(cursor.getColumnIndex(KEY_GAME_NAME))

                val game = Game(id = id, gameName = name)
                gamesList.add(game)
            } while (cursor.moveToNext())
        }

        return gamesList
    }

}