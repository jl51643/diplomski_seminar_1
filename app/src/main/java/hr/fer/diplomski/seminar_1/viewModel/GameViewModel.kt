package hr.fer.diplomski.seminar_1.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hr.fer.diplomski.seminar_1.database.DatabaseHandler
import hr.fer.diplomski.seminar_1.model.Card
import hr.fer.diplomski.seminar_1.model.Cards
import hr.fer.diplomski.seminar_1.model.Game

class GameViewModel(private val databaseHandler: DatabaseHandler): ViewModel() {

    val gamesList: MutableLiveData<ArrayList<Game>> = MutableLiveData()

    val cards: MutableLiveData<Cards> = MutableLiveData()

    fun createGame(name: String): Game {
        return databaseHandler.createGame(name)
    }

    fun getGames() {
        gamesList.value = databaseHandler.getGames()
    }

    fun addCards(cards:ArrayList<Card>, gameId: Long): Cards {
        return databaseHandler.insertCards(cards, gameId)
    }

    fun getCards(gameId: Long) {
        cards.value = databaseHandler.getGameCards(gameId)
    }
}