package hr.fer.diplomski.seminar_1

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hr.fer.diplomski.seminar_1.database.DatabaseHandler
import hr.fer.diplomski.seminar_1.databinding.ActivityGamesListBinding
import hr.fer.diplomski.seminar_1.model.Game
import hr.fer.diplomski.seminar_1.viewModel.GameViewModel
import hr.fer.diplomski.seminar_1.viewModel.factory.GameViewModelFactory
import kotlin.math.log

class GamesListActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGamesListBinding
    private lateinit var gamesListAdapter: GamesListAdapter
    private lateinit var viewModel: GameViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGamesListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.title = "Available games"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val listOfGamesView = binding.listOfGames

        listOfGamesView.layoutManager = LinearLayoutManager(applicationContext)

        val decorator = DividerItemDecoration(applicationContext, LinearLayoutManager.VERTICAL)
        decorator.setDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.cell_divider)!!)
        listOfGamesView.addItemDecoration(decorator)

        val databaseHandler: DatabaseHandler = DatabaseHandler(applicationContext)
        val viewModelFactory = GameViewModelFactory(databaseHandler = databaseHandler)
        viewModel = ViewModelProvider(this, viewModelFactory).get(GameViewModel::class.java)


        gamesListAdapter = GamesListAdapter(viewModel)
        listOfGamesView.adapter = gamesListAdapter

        viewModel.gamesList.observe(this, Observer {
            gamesListAdapter.notifyDataSetChanged()
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.getGames()
    }

    inner class GamesListAdapter(gameViewModel: GameViewModel): RecyclerView.Adapter<GamesListAdapter.ViewHolder>() {

        private var listOfGames: GameViewModel = gameViewModel

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var gameName: TextView? = null
            init {
                gameName = itemView.findViewById(R.id.game_name)
            }

        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): GamesListAdapter.ViewHolder {
            val context = parent.context
            val inflater = LayoutInflater.from(context)
            val gameListElement = inflater.inflate(R.layout.layout_game_list_item, parent, false)
            val listOfGamesView = findViewById<RecyclerView>(R.id.listOfGames)

            gameListElement.setOnClickListener {

                val itemPosition = listOfGamesView.getChildLayoutPosition(it)
                val game = listOfGames.gamesList.value?.get(itemPosition)

                val intent: Intent =
                    Intent(this@GamesListActivity, GameActivity::class.java).putExtra("game", game).putExtra("position", itemPosition)

                startActivity(intent)
                gamesListAdapter.notifyDataSetChanged()
            }
            return ViewHolder(gameListElement)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.gameName?.text = listOfGames.gamesList.value!![position].gameName
        }

        override fun getItemCount(): Int {
            return if (listOfGames.gamesList.value != null) {
                listOfGames.gamesList.value!!.count()
            } else {
                0
            }
        }
    }

}