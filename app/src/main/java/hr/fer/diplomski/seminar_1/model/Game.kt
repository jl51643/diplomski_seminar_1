package hr.fer.diplomski.seminar_1.model

import java.io.Serializable

data class Game(var id: Long, var gameName: String): Serializable
