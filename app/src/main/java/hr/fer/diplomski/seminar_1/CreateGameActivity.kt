package hr.fer.diplomski.seminar_1

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import hr.fer.diplomski.seminar_1.databinding.ActivityCreateGameBinding
import hr.fer.diplomski.seminar_1.R
import hr.fer.diplomski.seminar_1.database.DatabaseHandler
import hr.fer.diplomski.seminar_1.model.Card
import hr.fer.diplomski.seminar_1.viewModel.GameViewModel
import hr.fer.diplomski.seminar_1.viewModel.factory.GameViewModelFactory

class CreateGameActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCreateGameBinding

    private lateinit var viewModel: GameViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCreateGameBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.title = "Create game"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val databaseHandler: DatabaseHandler = DatabaseHandler(applicationContext)
        val viewModelFactory = GameViewModelFactory(databaseHandler = databaseHandler)
        viewModel = ViewModelProvider(this, viewModelFactory).get(GameViewModel::class.java)

        binding.creteGameBtn.setOnClickListener {

            val game = viewModel.createGame(binding.gameName.text.toString())

            val cards: ArrayList<Card> = ArrayList()

            cards.add(Card(word = binding.word1.text.toString(), translation = binding.translation1.text.toString()))
            cards.add(Card(word = binding.word2.text.toString(), translation = binding.translation2.text.toString()))
            cards.add(Card(word = binding.word3.text.toString(), translation = binding.translation3.text.toString()))
            cards.add(Card(word = binding.word4.text.toString(), translation = binding.translation4.text.toString()))

            viewModel.addCards(cards, game.id)

            Toast.makeText(this, "", Toast.LENGTH_SHORT).show()

            val intent: Intent = Intent(this@CreateGameActivity, GamesListActivity::class.java)
            startActivity(intent)
        }
    }
}