package hr.fer.diplomski.seminar_1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import hr.fer.diplomski.seminar_1.databinding.ActivityWelcomeBinding

class WelcomeActivity : AppCompatActivity() {

    lateinit var binding: ActivityWelcomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWelcomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar?.title = "Memory game"

        binding.btnPlayGame.setOnClickListener{

            val intent = Intent(this, GamesListActivity::class.java)
            startActivity(intent)
        }

        binding.btnCreateGame.setOnClickListener{
            val intent = Intent(this, CreateGameActivity::class.java)
            startActivity(intent)
        }


    }
}