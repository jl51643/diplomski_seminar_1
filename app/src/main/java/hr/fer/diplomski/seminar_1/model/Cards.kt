package hr.fer.diplomski.seminar_1.model

import java.io.Serializable

data class Cards(var gameId: Long, var cards: ArrayList<Card>): Serializable
